package com.planetspark.app.learnline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import com.google.android.youtube.player.YouTubePlayer;
import com.planetspark.app.learnline.R;
import com.planetspark.app.learnline.activity.WebViewActivity;
import com.planetspark.app.learnline.activity.YouTubePlayerActivity;

import net.cachapa.expandablelayout.ExpandableLayout;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.MyViewHolder>{
    RecyclerView recyclerView;
    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;
    Context context;
    public MainRecyclerViewAdapter(Context context1,RecyclerView recyclerView1){
        this.context=context1;
        this.recyclerView=recyclerView1;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainRecyclerViewAdapter.MyViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
                                        ExpandableLayout.OnExpansionUpdateListener {
        private TextView expandButton;
        private ExpandableLayout expandableLayout;
        CardView webCardView,youTubeView1,youTubeView2;

        public MyViewHolder(View itemView) {
            super(itemView);
            expandButton = itemView.findViewById(R.id.expand_button);
            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            webCardView=itemView.findViewById(R.id.webCardView);
            youTubeView1=itemView.findViewById(R.id.youTubeView1);
            youTubeView2=itemView.findViewById(R.id.youTubeView2);
            expandableLayout.setInterpolator(new OvershootInterpolator());
            expandableLayout.setOnExpansionUpdateListener(this);
            expandButton.setOnClickListener(this);
            webCardView.setOnClickListener(this);
            youTubeView1.setOnClickListener(this);
            youTubeView2.setOnClickListener(this);
        }

        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
            expandButton.setText(position+1 + ". Tap to expand");
            expandButton.setSelected(isSelected);
            expandableLayout.setExpanded(isSelected, false);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Log.d("ExpandableLayout", "State: " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.expand_button:
                    MyViewHolder holder = (MyViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
                    if (holder != null) {
                        holder.expandButton.setSelected(false);
                        holder.expandableLayout.collapse();
                    }

                    int position = getAdapterPosition();
                    if (position == selectedItem) {
                        selectedItem = UNSELECTED;
                    } else {
                        expandButton.setSelected(true);
                        expandableLayout.expand();
                        selectedItem = position;
                    }
                    break;

                case R.id.webCardView:
                    Intent intent=new Intent(context, WebViewActivity.class);
                    context.startActivity(intent);
                    break;

                case R.id.youTubeView1:
                    Intent intent1=new Intent(context, YouTubePlayerActivity.class);
                    intent1.putExtra("video_id","_0NsV-tol7s");
                    context.startActivity(intent1);
                    break;

                case R.id.youTubeView2:
                    Intent intent2=new Intent(context, YouTubePlayerActivity.class);
                    intent2.putExtra("video_id","-QU8xG-molE");
                    context.startActivity(intent2);
                    break;
            }
        }
    }
}
