package com.planetspark.app.learnline.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.planetspark.app.learnline.R;
import com.planetspark.app.learnline.helper.SimpleDividerItemDecoration;
import com.planetspark.app.learnline.adapter.MainRecyclerViewAdapter;

public class MainActivity extends AppCompatActivity {
    RecyclerView mainRecyclerView;
    MainRecyclerViewAdapter mainRecyclerViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainRecyclerView=findViewById(R.id.main_recycler_view);
        mainRecyclerViewAdapter=new MainRecyclerViewAdapter(this,mainRecyclerView);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setHasFixedSize(true);
        mainRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        mainRecyclerView.setAdapter(mainRecyclerViewAdapter);
    }
}
